#ifndef TrackingPerf_H
#define TrackingPerf_H 1

#include <EventLoop/Algorithm.h>
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include "TrigDecisionTool/TrigDecisionTool.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODHIEvent/HIEventShapeContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTrigger/TrigCompositeContainer.h"
#include "xAODTrigger/EnergySumRoI.h"

#include "TH1.h"
#include "TH2.h"

#include <string>
#include <stdlib.h>

class TrackingPerf: public EL::Algorithm { 
 public: 
  TrackingPerf();
  TrackingPerf(const TrackingPerf& alg);
  virtual ~TrackingPerf(); 

  virtual EL::StatusCode initialize();     //once, before any input is loaded
  virtual EL::StatusCode beginInputFile(); //start of each input file, only metadata loaded
  virtual EL::StatusCode execute();        //per event
  virtual EL::StatusCode finalize();       //once, after all events processed

  virtual EL::StatusCode setupJob(EL::Job& job);
  virtual EL::StatusCode changeInput(bool firstFile);
	virtual EL::StatusCode fileExecute();
	virtual EL::StatusCode postExecute();

    // this is needed to distribute the algorithm to the workers
  ClassDef(TrackingPerf, 1);
  
}; 

#endif //> !TrackingPerf_H
