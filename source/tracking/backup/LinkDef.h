#include <tracking_5TeV/Tracking5TeV.h>
#include <tracking_5TeV/BaseClass.h>
#include <tracking_5TeV/OverlayTreeMaker.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#endif

#ifdef __CINT__
#pragma link C++ class Tracking5TeV+;
#pragma link C++ class BaseClass;
#pragma link C++ class OverlayTreeMaker+;
#endif
