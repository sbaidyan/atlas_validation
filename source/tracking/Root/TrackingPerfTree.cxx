#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include "tracking/TrackingPerfTree.h"


ClassImp(TrackingPerfTree)

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK( CONTEXT, EXP )			\
  do {							\
    if( ! EXP.isSuccess() ) {				\
      Error( CONTEXT,					\
	     XAOD_MESSAGE( "Failed to execute: %s" ),	\
	     #EXP );					\
      return EL::StatusCode::FAILURE;			\
    }							\
  } while( false )


TrackingPerfTree::TrackingPerfTree()
{
}

TrackingPerfTree::TrackingPerfTree(const TrackingPerfTree& alg)
{
}


TrackingPerfTree::~TrackingPerfTree() {}



EL::StatusCode TrackingPerfTree::setupJob(EL::Job& job)
{
	// let's initialize the algorithm to use the xAODRootAccess package
	job.useXAOD();
	
	EL_RETURN_CHECK( "setupJob()", xAOD::Init() ); // call before opening first file
	std::cout << " Job setup done!" << std::endl;
	
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerfTree::histInitialize()
{
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerfTree::fileExecute()
{
	// Here you do everything that needs to be done exactly once for every
	// single file, e.g. collect a list of all lumi-blocks processed
  
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerfTree::postExecute()
{
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerfTree::changeInput(bool firstFile)
{
	return EL::StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerfTree::initialize()
{
  Info("initialize()", "Initializing %s...", name().c_str());
	xAOD::TEvent* event = wk()->xaodEvent();
	Info("initialize()", "Number of events = %lli", event->getEntries()); // print long long int

  tree = new TTree("analysis", "My analysis ntuple");
  TFile *outputFile = wk()->getOutputFile(output_file_name.c_str());
  tree->SetDirectory(outputFile);

  tree->Branch("evt_counter", &evt_counter);
  tree->Branch("lb", &lb);
  tree->Branch("bcid", &bcid);
  tree->Branch("run_number", &run_number);

  tree->Branch("passedLAr", &passedLAr);
  tree->Branch("passedTile", &passedTile);
  tree->Branch("passedSCT", &passedSCT);
  tree->Branch("passedCore", &passedCore);

  tree->Branch("nvertex_objects", &nvertex_objects);
  tree->Branch("nvertex", &nvertex);
  tree->Branch("npvertex", &npvertex);
  tree->Branch("vertex_z", &vertex_z);
  tree->Branch("primary_vertex_z", &primary_vertex_z);
  tree->Branch("distance_to_pv", &distance_to_pv);

  tree->Branch("ntracks", &ntracks);
  tree->Branch("trk_pt", &trk_pt);
  tree->Branch("trk_eta", &trk_eta);
  tree->Branch("trk_phi", &trk_phi);
  tree->Branch("trk_theta", &trk_theta);
  tree->Branch("trk_d0", &trk_d0);
  tree->Branch("trk_z0", &trk_z0);
  tree->Branch("trk_vz", &trk_vz);
  tree->Branch("trk_z0_pv", &trk_z0_pv);
  tree->Branch("trk_chi2", &trk_chi2);
  tree->Branch("trk_ndof", &trk_ndof);

  tree->Branch("trk_theta_err", &trk_theta_err);
  tree->Branch("trk_d0_err", &trk_d0_err);
  tree->Branch("trk_z0_err", &trk_z0_err);

  tree->Branch("n_pix_hits", &n_pix_hits);
  tree->Branch("n_pix_holes", &n_pix_holes);
  tree->Branch("n_sh_pix_hits", &n_sh_pix_hits);
  tree->Branch("n_sct_hits", &n_sct_hits);
  tree->Branch("n_sct_holes", &n_sct_holes);
  tree->Branch("n_sh_sct_hits", &n_sh_sct_hits);
  tree->Branch("n_trt_hits", &n_trt_hits);
  tree->Branch("n_ibl_hits", &n_ibl_hits);
  tree->Branch("n_bl_hits", &n_bl_hits);
  tree->Branch("n_exp_ibl_hits", &n_exp_ibl_hits);
  tree->Branch("n_exp_bl_hits", &n_exp_bl_hits);

  return StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerfTree::finalize()
{
  Info("finalize()", "Finalizing %s", name().c_str());
  return StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerfTree::execute()
{
  //---- EVENTS ---//
  // getting event info
  const xAOD::EventInfo* ptrEvt = 0;
  EL_RETURN_CHECK("execute()", evtStore()->retrieve(ptrEvt, "EventInfo"));
  Info("execute()", "EventNo: %lli", ptrEvt->eventNumber());

  evt_counter++;
  lb = ptrEvt->lumiBlock();
  bcid = ptrEvt->bcid();
  run_number= ptrEvt->runNumber();

  passedLAr = ptrEvt->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error;
  passedTile = ptrEvt->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error;
  passedSCT = ptrEvt->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error;
  passedCore = ptrEvt->isEventFlagBitSet(xAOD::EventInfo::Core, 18);

  //---- VERTICES ---//
  // getting all vertices
  const xAOD::VertexContainer* vertices = nullptr;
  EL_RETURN_CHECK("execute()", evtStore()->retrieve(vertices, "PrimaryVertices"));
  std::vector<const xAOD::Vertex*> allvertices(vertices->begin(), vertices->end());
  nvertex_objects = allvertices.size();

  // getting all but NoVtx type vertices
  std::vector<const xAOD::Vertex*> fvertices;
  for (const xAOD::Vertex* vtx : allvertices)
    if (vtx && vtx->vertexType() != xAOD::VxType::NoVtx)
      fvertices.push_back(vtx);
  nvertex = fvertices.size();

  // filling z of vertices and the primary vertex
  std::unique_ptr<xAOD::Vertex> pv;
  for (const xAOD::Vertex* vtx : fvertices)
  {
    vertex_z.push_back(vtx->z());
    if (vtx->vertexType() == xAOD::VxType::PriVtx)
    {
      primary_vertex_z.push_back(vtx->z());
      pv = std::make_unique<xAOD::Vertex>(*vtx);
    }
    else
      primary_vertex_z.push_back(-999);
  }

  // filling distances to the primary vertex
  for (const xAOD::Vertex* vtx : fvertices)
    if (pv and vtx->vertexType() != xAOD::VxType::PriVtx)
      distance_to_pv.push_back(vtx->z() - pv->z());
    else
      distance_to_pv.push_back(-999);

  //---- TRACKS ---//
  // getting all tracks
  const xAOD::TrackParticleContainer* tracks = nullptr;
  EL_RETURN_CHECK("execute()", evtStore()->retrieve(tracks, "InDetTrackParticles"));
  std::vector<const xAOD::TrackParticle*> ftracks(tracks->begin(), tracks->end());
  ntracks = ftracks.size();

  // iterating over all tracks
  for (const xAOD::TrackParticle* track: ftracks)
  {
    // filling basic track parameters
    trk_pt.push_back(track->pt() * 1e-3);
    trk_eta.push_back(track->eta());
    trk_phi.push_back(track->phi());
    trk_theta.push_back(track->theta());
    trk_d0.push_back(track->d0());
    trk_z0.push_back(track->z0());
    trk_vz.push_back(track->vz());
    if (pv) 
      trk_z0_pv.push_back(track->z0() + track->vz() - pv->z());
    else
      trk_z0_pv.push_back(-999);
    trk_chi2.push_back(track->chiSquared());
    trk_ndof.push_back(track->numberDoF());

    // the type of matrix is DefiningParameters_t
    auto covMatrix = track->definingParametersCovMatrix();
    trk_theta_err.push_back(sqrt(covMatrix(3, 3)));
    trk_d0_err.push_back(sqrt(covMatrix(0, 0)));
    trk_z0_err.push_back(sqrt(covMatrix(1, 1)));
    
    auto getAux = [&] (const char* name) {
      return track->auxdata<unsigned char>(name);
    };

    // filling hits and holes
    n_pix_hits.push_back(getAux("numberOfPixelHits") + getAux("numberOfPixelDeadSensors"));
    n_pix_holes.push_back(getAux("numberOfPixelHoles"));
    n_sh_pix_hits.push_back(getAux("numberOfPixelSharedHits"));
    n_sct_hits.push_back(getAux("numberOfSCTHits") + getAux("numberOfSCTDeadSensors"));
    n_sct_holes.push_back(getAux("numberOfSCTHoles"));
    n_sh_sct_hits.push_back(getAux("numberOfSCTSharedHits"));
    n_trt_hits.push_back(getAux("numberOfTRTHits"));
    n_ibl_hits.push_back(getAux("numberOfInnermostPixelLayerHits"));
    n_bl_hits.push_back(getAux("numberOfNextToInnermostPixelLayerHits"));
    n_exp_ibl_hits.push_back(getAux("expectInnermostPixelLayerHit"));
    n_exp_bl_hits.push_back(getAux("expectNextToInnermostPixelLayerHit"));
  }

  tree->Fill();

  vertex_z.clear();
  primary_vertex_z.clear();
  distance_to_pv.clear();

  trk_pt.clear();
  trk_eta.clear();
  trk_phi.clear();
  trk_theta.clear();
  trk_d0.clear();
  trk_z0.clear();
  trk_vz.clear();
  trk_z0_pv.clear();
  trk_chi2.clear();
  trk_ndof.clear();

  trk_theta_err.clear();
  trk_d0_err.clear();
  trk_z0_err.clear();

  n_pix_hits.clear();
  n_pix_holes.clear();
  n_sh_pix_hits.clear();
  n_sct_hits.clear();
  n_sct_holes.clear();
  n_sh_sct_hits.clear();
  n_trt_hits.clear();
  n_ibl_hits.clear();
  n_bl_hits.clear();
  n_exp_ibl_hits.clear();
  n_exp_bl_hits.clear();

  return StatusCode::SUCCESS;
}

EL::StatusCode TrackingPerfTree::beginInputFile()
{
  return StatusCode::SUCCESS;
}
