#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/ScanDir.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/CondorDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "EventLoopGrid/GridDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include "EventLoop/OutputStream.h"
#include "EventLoopAlgs/NTupleSvc.h"

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AnaAlgorithm/AnaAlgorithmConfig.h>

#include <string>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <exception>

#include <TROOT.h>
#include <TSystem.h>
#include <TChain.h>
#include <TFile.h>
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include "tracking/TrackingPerf.h"
#include "tracking/TrackingPerfTree.h"


int main(int argc, char *argv[])
{
	
	//configuration
	std::string analysis_type;
	int num_evt;
	std::string input_directory;
	std::string submitDir;
	std::string InDS;
	std::string OutDS;
	bool isGridJob;
	int nFilesPerJob;
	bool gridMerge;
	std::string output_file_name="ntuple";
	std::string grid_configuration="";

	//Boost configuration
	//1) command line only: options can only be given on command line, not in config file
	boost::program_options::options_description cmd_only_options("command line only options");
	std::string config_file_name;

	cmd_only_options.add_options() //note unusual syntax when adding options!
		("help,h", "produce help message")
		("config,c", boost::program_options::value<std::string>(&config_file_name),"name of configuration file");
	
	//2) main options: most likely to be set by user, can be specified both via command line or config
	//explaination is included in help message
	boost::program_options::options_description main_options("main options");
	
	main_options.add_options()
		("analysis_type",boost::program_options::value<std::string>(&analysis_type)->default_value("hist"),"what analysis to use")
		("output_file",boost::program_options::value<std::string>(&output_file_name)->default_value("ntuple"),"name of output root file")
        ("num_evt,n",boost::program_options::value<int>(&num_evt)->default_value(-1),"number of events, -1 runs all events")
		("input_directory",boost::program_options::value<std::string>(&input_directory)->default_value("/afs/cern.ch/user/s/sbaidyan/atlas/zmmee_pPb8p16TeV_14aug23/inputData/"),"name of input directory containing all datasets")
		("submit_directory",boost::program_options::value<std::string>(&submitDir)->default_value("submitDir"),"name of output directory")
		("InDS,i",boost::program_options::value<std::string>(&InDS)->default_value(""),"InDS for grid job")
		("OutDS,o",boost::program_options::value<std::string>(&OutDS)->default_value(""),"OutDS for grid job")
		("isGridJob",boost::program_options::value<bool>(&isGridJob)->default_value(0),"is it grid job?")
		("nFilesPerJob",boost::program_options::value<int>(&nFilesPerJob)->default_value(1),"Number of files per grid job")
		("gridMerge",boost::program_options::value<bool>(&gridMerge)->default_value(1),"Merge grid results")
		("grid_configuration",boost::program_options::value<std::string>(&grid_configuration)->default_value(""),"Settings for grid configuration")
		;
	
	//combine options types for parsing
	//all options may be specified on command line
	boost::program_options::options_description cmdline_options; 
	cmdline_options.add(cmd_only_options).add(main_options);
	
	//all options except command line only may be specified in config file
	boost::program_options::options_description config_options; 
	config_options.add(main_options);
	
	boost::program_options::variables_map vm;

	//first parse command line
	try
	{
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, cmdline_options), vm);
		boost::program_options::notify(vm);
	}
	catch(std::exception& e)
	{
		std::cerr << "Bad command line argument" << std::endl;
		std::cerr << e.what() << std::endl;
		return 1;
	}

	//if config was specified, also parse config file
	if(vm.count("config"))
	{
		std::ifstream config_stream(config_file_name.c_str());
		try
		{
			boost::program_options::store(boost::program_options::parse_config_file(config_stream, cmdline_options), vm);
			boost::program_options::notify(vm);
		}
		catch(std::exception& e)
		{
			std::cerr << "Bad config file argument" << std::endl;
			std::cerr << e.what() << std::endl;
			return 1;
		}
	}

	std::cout << std::endl << "*********Configuration**********" << std::endl;
	std::cout << "Output directory: " << submitDir << std::endl;

	if (strcmp (grid_configuration.c_str(),"") != 0)  std::cout << "Additional grid configuration: " << grid_configuration.c_str() << std::endl;
	
	std::cout << "********************************" << std::endl << std::endl;
	
	// Set up the job for xAOD access:
	xAOD::Init().ignore();

	// Construct the samples to run on:
	SH::SampleHandler sh;
	if (isGridJob){
		SH::scanRucio (sh, InDS.c_str());
		sh.setMetaString( "nc_grid_filter", "*AOD*");
	}
	else {
	  //local run
	  SH::ScanDir().filePattern("*").scan(sh, input_directory + InDS);
	}
	sh.setMetaString( "nc_tree", "CollectionTree" );

 	// Print what we found:
	sh.print(); 

	// Create an EventLoop job:
	std::cout << "Creating EventLoop job" << std::endl;
	EL::Job job;
	 
	//Set outputFile
	EL::OutputStream output(output_file_name.c_str());
	job.outputAdd(output);
	EL::NTupleSvc *ntuple = new EL::NTupleSvc(output_file_name.c_str());
	job.algsAdd(ntuple);
	job.sampleHandler(sh);
	std::cout << "Seting maximum events to " << num_evt << std::endl;
	job.options()->setDouble(EL::Job::optMaxEvents, num_evt);
	job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);


	// Add our analysis to the job:
	std::cout << "Add our analysis to the job" << std::endl;
	if (analysis_type == "hist")
	{
		TrackingPerf* alg = new TrackingPerf();
		job.algsAdd( alg );
	}
	else if (analysis_type == "tree")
	{
		TrackingPerfTree* alg = new TrackingPerfTree();
		alg->output_file_name = output_file_name;
		job.algsAdd( alg );
	}
	else
	{
		throw std::runtime_error("Wrong analysis type!");
	}

	// Run the job using the local/direct driver:
	std::cout << "Run the job" << std::endl;
	//Split level protection    
    job.options()->setString(EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_athena);
	

	if(isGridJob){
		EL::PrunDriver driver;
		driver.options()->setString("nc_outputSampleName", OutDS.c_str());
		if (nFilesPerJob > 0)
			driver.options()->setDouble("nc_nFilesPerJob", nFilesPerJob);
		driver.options()->setString("nc_cmtConfig", "x86_64-centos7-gcc11-opt");
		if (gridMerge)
			driver.options()->setDouble(EL::Job::optGridMergeOutput, 1); //run merging jobs for all samples before downloading (recommended)
		if (strcmp (grid_configuration.c_str(),"") != 0) 
			job.options()->setString (EL::Job::optSubmitFlags, grid_configuration.c_str()); //allow task duplication
		driver.submitOnly( job, submitDir );	
	}
	else {
	  //local run
	  EL::DirectDriver driver;
		driver.submit( job, submitDir );
	}

	std::cout << "We are done!" << std::endl << std::endl;
}

