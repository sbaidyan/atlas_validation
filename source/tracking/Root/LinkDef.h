#include <tracking/TrackingPerf.h>
#include <tracking/TrackingPerfTree.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#endif

#ifdef __CINT__
#pragma link C++ class TrackingPerf+;
#pragma link C++ class TrackingPerfTree+;
#endif
